#!/usr/bin/env python
# -*- coding: utf-8 -*-
#==========================================================================
# Program by Erik Johannes B. L. G. Husom on 2018-12-22 for Python 3.8
# Description: Scanning workfout
#
# TODO: Plot a workout on a map when peak is found, in order to easily confirm.
#
# Accepted file formats:
# - .json
#
# USAGE:
# $ python3 peakscan.py
#==========================================================================

# IMPORT STATEMENTS
import numpy as np
import sys, os, time, io, json
import pandas as pd
import matplotlib.pyplot as plt
from xml.dom import minidom
import plotly.express as px
import plotly.io as pio

class Peakscan():

    def __init__(self, lat, lon, filepath):

        self.lat = lat
        self.lon = lon
        self.filepath = filepath
        self.tolerance = 1e-3
        self.counter = 0

        for entry in os.listdir(self.filepath):
            
            f = self.filepath + "/" + entry
            self.file = entry

            filename, ext = os.path.splitext(entry)

            # Read json-file
            if (ext=='.json'):
                try:
                    data = json.load(io.open(f, 'r', encoding='utf-8-sig'))
                except UnicodeDecodeError:
                    continue
                # Read data samples
                try:
                    numberOfSamples = len(data["RIDE"]["SAMPLES"])
                except KeyError:
                    continue

                latitude = []
                longitude = []

                # Check if the first coordinate is within one degree in both
                # longitude and latitue. If not, the file can be safely
                # discarded, because it is too far from the peak.
                try:
                    first_lat = data["RIDE"]["SAMPLES"][0]["LAT"]
                    first_lon = data["RIDE"]["SAMPLES"][0]["LON"]

                    if abs(self.lat - first_lat) > 1 and abs(self.lon -
                            first_lon) > 1:
                        continue

                except KeyError:
                    continue

                for i in range(numberOfSamples):
                    latitude.append(data["RIDE"]["SAMPLES"][i]["LAT"])
                    longitude.append(data["RIDE"]["SAMPLES"][i]["LON"])

            elif (ext == ".gpx"):

                with open(f, "r") as f:
                    data = f
                    xmldoc = minidom.parse(data)

                track = xmldoc.getElementsByTagName('trkpt')
                elevation_xml = xmldoc.getElementsByTagName('ele')
                datetime_xml = xmldoc.getElementsByTagName('time')
                sample_count = len(track)

                longitude = []
                latitude = []
                # Check if the first coordinate is within one degree in both
                # longitude and latitue. If not, the file can be safely
                # discarded, because it is too far from the peak.
                try:
                    first_lon, first_lat = track[0].attributes['lon'].value, track[0].attributes['lat'].value

                    if abs(self.lat - first_lat) > 1 and abs(self.lon -
                            first_lon) > 1:
                        continue

                except KeyError:
                    continue

                for s in range(sample_count):
                    lon, lat = track[s].attributes['lon'].value,track[s].attributes['lat'].value
                    longitude.append(float(lon))
                    latitude.append(float(lat))

            else:
                print("Wrong file format. Only .json supported")
                continue

            self.latitude = np.array(latitude)
            self.longitude = np.array(longitude)

            found = self.peakscan()

            if found:
                df = pd.DataFrame({'Latitude': self.latitude, 'Longitude':
                    self.longitude})


                fig = px.scatter_mapbox(df,
                        lat="Latitude",
                        lon="Longitude",
                        zoom=10,
                        height=800,
                        width=800)

                fig.update_layout(title=filename)
                fig.update_layout(mapbox_style="open-street-map")
                # pio.write_image(fig, f"{filename}.png")
                pio.write_html(fig, f"{filename}.html")
                fig.show()

                print("Count:", self.counter)

    def peakscan(self):

        for lat, lon in zip(self.latitude, self.longitude):

            if abs(self.lat - lat) < self.tolerance:
                if abs(self.lon - lon) < self.tolerance:
                    print("Peak visit registered:", self.file)
                    self.counter += 1
                    return True

        return False

if __name__ == '__main__':
    try:
        lat = float(sys.argv[1])
        lon = float(sys.argv[2])
        files = sys.argv[3]
    except IndexError:
        print('Give name of workout file as command line argument.')
        sys.exit(1)

    workout = Peakscan(lat, lon, files)

